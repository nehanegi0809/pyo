student_name_file = open("../Students", "r+")
print(student_name_file.readable())  # prebuilt function to give a boolean variable if the file is readable or not
print(student_name_file.read())
for student_name in student_name_file:
    print(student_name_file.readline()[1])
print(student_name_file.readline())
student_name_file.close()

new_file = open("../Files/Name_Student", "a")
new_file.write("Kenji Middle School")
new_file.write("Simon Junior High")
new_file.close()


second_file = open("../Files/Names", "w")
second_file.write("Ayesha High School")
second_file.write("Matt Junior High")
second_file.close()