FullNameDay = {
    "Sun" : "Sunday",
    "Mon" : "Monday",
    "Tue" : "Tuesday",
    "Wed" : "Wednesday",
    "Thu" : "Thursday",
    "Fri" : "Friday",
    "Sat" : "Saturday",
}

print(FullNameDay["Mon"])
print(FullNameDay.get("Tue"))
print(FullNameDay.get("Helen"))
print(FullNameDay.get("Helen","Helen is not a valid key"))