from math import *

num = -5.1
#  generic number printing statements examples
print(2.1)
print(-2.1)
print(abs(2.1))
print(abs(-2.1))
print(num)
print(abs(num))  # absolute value of num
print(1 + num)  # adding two numbers inside the print statement

#  calculations with numbers examples
add_num = 2 + num
print(add_num)  # calculating sum of 2 numbers and printing the value of the sum variable

print(2 * num)  # multiplying two numbers inside the print statement

product_num = 3 * num
print(product_num)  # calculating product of 2 numbers and printing the value of the product variable

print(2 * (4 + 5) - num)  # basic BODMAS test
print(2 / (6 * num) + 4)  # maybe-not-so-basic BODMAS test
print(2 + 4 / (6 * num) - add_num + product_num)  # musing calculated values for BODMAS test

print(5 % 2)  # prints remainder

# interesting bits for printing a number
new_num = 8
#  this statement gives error
#  >print("My favorite number is" + new_num)
#  why? since new_num is not a string and print statment cannot
#  concat-inate an integer to a string
#  "Tip" Python doesn't support multi-line comment hence '##' and '##'

print("My favorite number is " + str(new_num))  # this works we convereted new_num to string

#  basic operations with numbers

print(pow(6, 1))  # 6 raised to the power 1
print(pow(6, 2))  # 6 raised to the power obviously 2

print(max(5, 2))  # max of thr two nums
print(max(2, 4, 1, 8))  # max of the given numbers
#  'Tip' maximum number of arguments of a function in python is now unlimited which was previously only 255
print(max(1, 1))  # comparing equal numbers or a tie case

print(min(5, 2))  # min of thr two nums
print(min(2, 4, 1, 8))  # min of the given numbers
print(max(2, 2))  # comparing equal numbers or a tie case

print(round(6.2))  # round off
print(round(6.7))  # also round off

print(floor(6.8))  # round off to the lowest value, using this function of the math module
print(ceil(6.8))  # round off to the highest value, also using math module
print(sqrt(144))  #square root

