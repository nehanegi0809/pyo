is_hungry = True
if is_hungry:
    print("you are hungry :D")
else:
    print("You are not hungry :(")

is_hungry = False

if is_hungry:
    print("You are hungry :D")
else:
    print("You are not hungry :(")

is_hungry = True
is_smart = True
if is_hungry or is_smart:
    print("You are hungry or smart or both :)")
else:
    print("Your are neither hungry nor smart :(")

is_hungry = True
is_smart: bool = True
if is_hungry and is_smart:
    print("You are hungry and smart :)")
else:
    print("You are either not hungry or you aren't smart or both")


is_smart = False
if is_hungry and is_smart:
    print("You are hungry and smart :)")
elif is_hungry and not(is_smart):
    print("You are hungry and not smart")


#  Comparisons
def max_number(num1, num2, num3):
    if num1 >= num2 and num1 >= num3:
        return num1
    elif num2 >= num1 and num2 >= num3:
        return num2
    else:
        return num3

print(max_number(2,24,6))