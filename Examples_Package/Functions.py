def greet_hi():
    print("Hello there!")

print("Dear Guests")
greet_hi()
print("Our greetings")

#  function with a parameter
def new_greet(name):
    print("Hello there " + name + "!")

new_greet("Pat")
new_greet("Matt")

#  function without return statement
def sqr_a(number):
    number*number

#  function with a return statement
def sqr(number):
    return number*number

Ans = sqr(8)
print(Ans)
print(sqr(9))