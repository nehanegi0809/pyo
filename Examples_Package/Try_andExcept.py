#  Example 1
try:
    store_num = int(input("Type a number:"))
    print(store_num)
except:
    print("You've entered a wrong input.")

#  Example 2
try:
    Num = 1 / 0
    Store_num = int(input("Type a number:"))
    print(store_num)
except ZeroDivisionError:
    print("The number is divided by zero")
except ValueError:
    print("You entered a wrong input")
