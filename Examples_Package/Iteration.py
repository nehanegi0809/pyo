# while loop
i = 1
while i <= 12:
    print(i)
    i+=1

# for loop
for characters in "Gentleman":
    print(characters)

people = ["Matt", "Jake", "Helen", "Martha", "Ayesha", "Tim", "Sam"]

for person in people:
    print(person)

for index in range(6):
    print(index)

for index in range(2,6):
    print(index)

for index in range(len(people)):
    print(people[index])

for index in range(4):
    if index == 0:
        print("First person")
    else:
        print("Not first")