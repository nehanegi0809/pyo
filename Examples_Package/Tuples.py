Location_coordinates = (8, 9)  # a tuple object which is immutable
print(Location_coordinates)
print(Location_coordinates[0])
Location_coordinates[1] = 5
#  print(Location_coordinates) gives error because tuples are immutable
