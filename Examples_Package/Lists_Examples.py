BFF = ["name1", "name2", "name3", "name4"]
print(BFF)
print(BFF[2])
print(BFF[-1])  # accessing elements of an array from the last index
print(BFF[1:])  # printing elements after the specified index
print(BFF[1:3])  # printing elements between index 1 to 3, including element at index 1
BFF[2] = "changedName"
print(BFF)  # element at index 2 replaced

#  Using simple prebuilt code in lists

Grades_Students = [3, 6, 8, 2, 9, 11]  # grades list
Students = ["Ana", "Max", "Andrei", "Raghav"]  # list with names of students
print(Students)
#  modifying the students list
Students.extend(Grades_Students)  # prints both the names of students and grades first all the names followed by grades
print(Students)  # our modified list

Students.append("Neha")  # added a new value at the end of the modified list
print(Students)

Students.insert(1, "Matt")
print(Students)  # replacing Max at index 1 with Matt

Students.remove("Matt")
print(Students)  # removed Matt from list

Students.remove("Max")
print(Students)  # removing Max from the list now the list is modified again

Students.pop()
print(Students)  # removing the last element from the modified list

print(Students.index("Ana"))  # printing index of an element

print(Students.count("Ana"))

Grades_Students.sort()
print(Grades_Students)  # sorting an integer list

Students.reverse()
print(Students)  # printing reverse of a list

Students2 = Students.copy()
print(Students2)  # printing copy of a list

Students.clear()
print(Students)  # clearing the whole list