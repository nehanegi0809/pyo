Clause: str = "A clause is a group of words" \
              "\nwhich has a subject and a verb unit. \n"  # definition of a clause
Phrase = "A phrase is a group of words " \
         "\nwhich has no subject and verb unit. \n"  # definition of a phrase
len_Clause = len(Clause)
len_Phrase = len(Phrase)
print(Clause + Phrase)
print("length of the string Clause ")
print(len_Clause)
print("length of the string Phrase")
print(len_Phrase)
print(Clause[4])
print(Phrase[5])
print(Clause.index('a'))
print(Phrase.index('a'))
#  gives error since m is not present in Clause print(Clause.index('m'))
#  gives error since q is not present in Phrase print(Phrase.index('q'))

#  fresh start now connected to the bitbucket repo at https://bitbucket.org/nehanegi0809/pyo.git
print(Clause.replace("group", "collection"))
print(Clause)  # word 'group' only replaced in printing statement the original string remains unchanged
print(Phrase.replace("group", "collection"))
print(Phrase)  # word 'group' only changed while printing in the previous statement
