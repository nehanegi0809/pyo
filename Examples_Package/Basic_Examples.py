Name = "Al"  # name is a variable
Age = "27"  # Age is also a variable
Progress1 = "test string"  # lower case string
Progress2 = "TEST STRING"  # upper case string
print(Name + " likes to play chess \n")
print("\"She is " + Age + " years old\"")
print("Chess is amazing")
print(Progress2.isupper())
print(Progress1.islower())
print(Progress1.isupper())
print(Progress2.islower())
print(len(Name))
print(len(Age))
print(len(Progress1))
print(len(Progress2))


