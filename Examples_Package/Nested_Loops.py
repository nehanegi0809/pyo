num_2dlist = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
    [0]
]

print(num_2dlist)
print(num_2dlist[0][0])
print(num_2dlist[2][1])
print(num_2dlist[3][0])

#  printing all the rows
for row in num_2dlist:
    print(row)

# printing all the elements of a 2D array
for row in num_2dlist:
    for col in row:
        print(col)